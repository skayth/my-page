<?php

add_action('widgets_init', 'hpa_register_func');

function hpa_register_func()
{
    register_widget("Hpa_Product");
}

class Hpa_Product extends WP_Widget
{

    public function __construct()
    {
        $args = array(
            'name' => 'HPA Products',
            'description' => 'Lorem ispum list'
        );
        parent::__construct('wfm_check', '', $args);
    }

    public function form($instance)
    {
        ?>

        <p>
        <input type="checkbox" name="<?php echo $this->get_field_name('chack'); ?>" 
        id="<?php echo $this->get_field_id('chack'); ?>" value="1"
        <?php checked($instance['chack'], 1) ?>>
        <label for="<?php echo $this->get_field_id('chack'); ?>">On | Of</label>
        </p>

        <p>
        <input type="text" id="<?php echo $this->get_field_id('count'); ?>"
        name="<?php echo $this->get_field_name('count'); ?>" placeholder="Pagination Count"
        class="widefat" value="<?php if (isset($instance['count'])) { echo $instance['count']; } ?>">
        </p>
        
        <p>
        <input type="text" id="<?php echo $this->get_field_id('card_count'); ?>"
        name="<?php echo $this->get_field_name('card_count'); ?>" placeholder="Card Count"
        class="widefat" value="<?php if (isset($instance['card_count'])) { echo $instance['card_count']; } ?>">
        </p>
       
        <?php
        
    }

    public function widget($argc, $instance)
    {
        if (!empty($instance['chack']) && !empty($instance['count']) && !empty($instance['card_count'])) {
        
            $posts = get_posts(array(
                'posts_per_page' => 5,
                'post_type' => 'product'
            ));

            $page_count = $instance['card_count'];
            $count = $instance['count'];

            echo '<div class="card border" id="hpa_card" style="width: 18rem;">';
            echo '<input type="text" class="form-control" id="hpa_search_input" placeholder="Search">';
            echo '<div class="position-relative" id="hpa_append_div"></div>';
            echo '<div id="hpa_for_remove">';
            for ($i = 0; $i<$page_count; $i++) {

                if ($i == count($posts)){ break; }

                if (!empty($posts)){

                $price = get_post_meta($posts[$i]->ID, 'price_input', true);
                $width = get_post_meta($posts[$i]->ID, 'width_input', true);
                $height = get_post_meta($posts[$i]->ID, 'height_input', true);
                $weight = get_post_meta($posts[$i]->ID, 'weight_input', true);
                $color = get_post_meta($posts[$i]->ID, 'color_input', true);
                
                if (get_option('hpa_title_on_of') == 1){
                    echo '<h5 class="card-title text-center">'. $posts[$i]->post_title .'</h5>';
                }
                echo '<div class="card-body">';
                if (get_option('hpa_image_on_of') == 1){
                    echo '<p class="card-text text-center">';
                    if (get_the_post_thumbnail($posts[$i]->ID, 'medium') != "") {
                        echo get_the_post_thumbnail($posts[$i]->ID, 'medium');
                    }else{
                        echo '<img class="mx-auto d-block" src="'.get_option( 'hpa_media_library' ).'">';
                }
                    echo '</p>';
                }
                if (get_option('hpa_content_on_of') == 1){
                    echo '<p class="card-text"><i>' . $posts[$i]->post_content . '</i></p>';
                }
                if (get_option('hpa_color_on_of') == 1){
                    echo '<p class="card-text pt5 text-center border border-dark" style="background: ' . $color . ';"><b>COLOR</b></p>';
                }
                if (get_option('hpa_price_on_of') == 1){
                    echo '<p class="card-text"><b>Price: - </b>' . $price . '</p>';
                }
                if (get_option('hpa_width_on_of') == 1){
                    echo '<p class="card-text"><b>Width: - </b>' . $width . '</p>';
                }
                if (get_option('hpa_height_on_of') == 1){
                    echo '<p class="card-text"><b>Height: - </b>' . $height . '</p>';
                }
                if (get_option('hpa_weight_on_of') == 1){
                    echo '<p class="card-text"><b>Weight: - </b>' . $weight . '</p>';
                }
                echo '</div>';

                }
            }
                echo '</div>';

                    $widget = get_option('widget_wfm_check');
                    $widget_count = $widget[2]['count'];
                    if (count($posts) > $page_count){
                        echo '<div id="pagination_buttons" class="text-center">';
                        $j = 1;
                        for ($i=0; $i<count($posts); $i=$i+$count) {
                            echo '<button data-val="'.$i.'" class="btn btn-primary ml-1 mr-1 mb-4 pagination_buttons">' . $j . '</button>';
                            $j++;
                        }
                        echo "</div>";
                    }
                echo "</div>";
            }
        
        }

}
