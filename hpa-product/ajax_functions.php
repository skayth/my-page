<?php

function wp_ajax_hpa_but(){

    $widget = get_option('widget_wfm_check');
    $widget_pagination = $widget[2]['count'];
    $widget_card = $widget[2]['card_count'];
    $page = $_POST['page'];
    $posts = array();
    $arr = array();
    $glob_arr = array();
    $k = 0;

    $posts_prod = get_posts(array(
        'posts_per_page'   => 5,
        'post_type' => 'product'
    ));
    
    foreach ($posts_prod as $post){
        if (preg_match('/'.$_POST['search'].'/i', $post->post_title)){
            array_push($posts, $post);
        }
    }

    $count = count($posts) - 1;

    for ($i = 0; $i<$widget_card; $i++){ 

    $title = $posts[$page]->post_title;
    if (get_the_post_thumbnail($posts[$page]->ID) != ""){
        $image = get_the_post_thumbnail($posts[$page]->ID, 'medium');
    }else{
        $image = '<img class="mx-auto d-block" src="'.get_option( 'hpa_media_library' ).'">';
    }
    $content = $posts[$page]->post_content;
    $color = get_post_meta($posts[$page]->ID, 'color_input', true);    
    $width = get_post_meta($posts[$page]->ID, 'width_input', true);
    $height = get_post_meta($posts[$page]->ID, 'height_input', true);
    $price = get_post_meta($posts[$page]->ID, 'price_input', true);
    $weight = get_post_meta($posts[$page]->ID, 'weight_input', true);

    $arr_settings = array(
        'hpa_title_on_of',
        'hpa_image_on_of',
        'hpa_content_on_of',
        'hpa_color_on_of',
        'hpa_width_on_of',
        'hpa_height_on_of',
        'hpa_price_on_of',
        'hpa_weight_on_of',
    );

    $arr_content = array($title, $image, $content, $color, $width, $height, $price, $weight);

    for ($j=0; $j<count($arr_settings); $j++){
        if (get_option($arr_settings[$j]) == 1){
            $arr[$k][$arr_settings[$j]] = $arr_content[$j];
        }else{
            $arr[$k][$arr_settings[$j]] = "undefined";
        }
    }
    $k++;

    if($page == $count){
        break;
    }
    
        $page++;
    }
    array_push($glob_arr, $arr);

        if (count($posts) > $widget_pagination){
            $buttons = '<div id="pagination_buttons" class="text-center">';
            $j = 1;
            for ($i=0; $i<count($posts); $i=$i+$widget_pagination) {
                $buttons .= '<button data-val="'.$i.'" class="btn btn-primary ml-1 mr-1 mb-4 pagination_buttons">' . $j . '</button>';
                $j++;
            }
            $buttons .= "</div>";
        }
        array_push($glob_arr, $buttons);

    echo json_encode($glob_arr);

    die();
}

add_action('wp_ajax_hpa_but', 'wp_ajax_hpa_but');



function wp_ajax_hpa_search()
{
    
    $widget = get_option('widget_wfm_check');
    $widget_pagination = $widget[2]['count'];
    $widget_card = $widget[2]['card_count'];
    $posts = array();
    $arr = array();
    $search = array();
    $k = 0;
    

    $posts_prod = get_posts(array(
        'posts_per_page'   => 5,
        'post_type' => 'product'
    ));
    
    foreach ($posts_prod as $post){
        if (preg_match('/'.$_POST['search'].'/i', $post->post_title)){
            array_push($posts, $post);
        }
    }

    

    foreach ($posts as $post){
        $title = $post->post_title;
        if (get_the_post_thumbnail($post->ID) != ""){
            $image = get_the_post_thumbnail($post->ID, 'medium');
        }else{
            $image = '<img class="mx-auto d-block" src="'.get_option( 'hpa_media_library' ).'">';
        }
        $content = $post->post_content;
        $color = get_post_meta($post->ID, 'color_input', true);
        $width = get_post_meta($post->ID, 'width_input', true);
        $height = get_post_meta($post->ID, 'height_input', true);
        $price = get_post_meta($post->ID, 'price_input', true);
        $weight = get_post_meta($post->ID, 'weight_input', true);

        array_push($search, $post->post_title);

        $arr_settings = array(
            'hpa_title_on_of',
            'hpa_image_on_of',
            'hpa_content_on_of',
            'hpa_color_on_of',
            'hpa_width_on_of',
            'hpa_height_on_of',
            'hpa_price_on_of',
            'hpa_weight_on_of',
        );
                
        $arr_content = array($title, $image, $content, $color, $width, $height, $price, $weight);

        for ($i=0; $i<count($arr_settings); $i++){
            if (get_option($arr_settings[$i]) == 1){
                $arr[$k][$arr_settings[$i]] = $arr_content[$i];
            }else{
                $arr[$k][$title][$arr_settings[$i]] = "undefined";
            }
        }
        $k++;

    }

        if (count($posts) > $widget_pagination){
            $buttons = '<div id="pagination_buttons" class="text-center">';
            $j = 1;
            for ($i=0; $i<count($posts); $i=$i+$widget_pagination) {
                $buttons .= '<button data-val="'.$i.'" class="btn btn-primary ml-1 mr-1 mb-4 pagination_buttons">' . $j . '</button>';
                $j++;
            }
            $buttons .= "</div>";
        }else{
            $buttons = '';
        }


    $glob_arr = array();

    if (count($search) > 0){
        array_push($glob_arr, $search);
        array_push($glob_arr, $arr);
        array_push($glob_arr, $widget_card);
        array_push($glob_arr, $buttons);
        echo json_encode($glob_arr);
    }else{
        return false;
    }
    
    die();
}

add_action('wp_ajax_hpa_search', 'wp_ajax_hpa_search');

function wp_ajax_hpa_div_click()
{
    $widget = get_option('widget_wfm_check');
    $widget_pagination = $widget[2]['count'];
    $posts = array();

    $posts_prod = get_posts(array(
        'posts_per_page'   => 5,
        'post_type' => 'product'
    ));
    
    foreach ($posts_prod as $post){
        if (preg_match('/'.$_POST['search'].'/i', $post->post_title)){
            array_push($posts, $post);
        }
    }

    if (count($posts) > $widget_pagination){
        $buttons = '<div id="pagination_buttons" class="text-center">';
        $j = 1;
        for ($i=0; $i<count($posts); $i=$i+$widget_pagination) {
            $buttons .= '<button data-val="'.$i.'" class="btn btn-primary ml-1 mr-1 mb-4 pagination_buttons">' . $j . '</button>';
            $j++;
        }
        $buttons .= "</div>";
    }else{
        $buttons = '';
    }
    
    echo json_encode($buttons);

    die();
}

add_action( 'wp_ajax_hpa_div_click', 'wp_ajax_hpa_div_click' );

function wp_ajax_hpa_update_image()
{
    echo $_POST['url'];

    die();
}

add_action( 'wp_ajax_hpa_update_image', 'wp_ajax_hpa_update_image' );