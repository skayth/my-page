jQuery(document).ready(function ($) {

    $('#pagination_buttons').on('click', '.pagination_buttons', function () {
        var btn_page = $(this).data('val');
        var search = $('#hpa_search_input').val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: hpa_prod_localize.url,
            data: {
                search: search,
                page: btn_page,
                action: 'hpa_but',
            },
            success: function (data) {

               var product = data[0];
               var buttons = data[1];

               var html = '';
               for(var i=0; i<product.length; i++){
                    if (product[i].hpa_title_on_of != "undefined"){
                        html += '<h5 class="card-title text-center">'+product[i].hpa_title_on_of+'</h5>';
                    }
                    html += '<div class="card-body">';
                    if (product[i].hpa_image_on_of != "undefined"){
                        html += '<p class="card-text">'+product[i].hpa_image_on_of+'</p>';
                    }
                    if (product[i].hpa_content_on_of != "undefined"){
                        html += '<p class="card-text"><i>'+product[i].hpa_content_on_of+'</i></p>';
                    }
                    if (product[i].hpa_color_on_of != "undefined"){
                        html += '<p class="card-text pt5 text-center border border-dark" style="background: '+product[i].hpa_color_on_of+';"><b>COLOR</b></p>'
                    }
                    if (product[i].hpa_width_on_of != "undefined"){
                        html += '<p class="card-text"><b>Price: - </b>'+product[i].hpa_width_on_of+'</p>';
                    }
                    if (product[i].hpa_height_on_of != "undefined"){
                        html += '<p class="card-text"><b>Width: - </b>'+product[i].hpa_height_on_of+'</p>';
                    }
                    if (product[i].hpa_price_on_of != "undefined"){
                        html += '<p class="card-text"><b>Height: - </b>'+product[i].hpa_price_on_of+'</p>';
                    }
                    if (product[i].hpa_weight_on_of != "undefined"){
                        html += '<p class="card-text"><b>Weight: - </b>'+product[i].hpa_weight_on_of+'</p>';
                    }
                    html += '</div>';
               }
                    
                  
                $('#hpa_for_remove').empty();
                $('#hpa_for_remove').append(html);

            }
        });
    });


    var hpa_data;
    var data_len;
    var buttons;
    $('#hpa_search_input').on('keyup click', function () {
        var search = $(this).val();
        $('#pagination_buttons').empty();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: hpa_prod_localize.url,
            data: {
                search: search,
                action: 'hpa_search',
            },
            success: function (data) {
                $('#hpa_append_div').empty();
                if(typeof data[0] != "undefined"){
                    for (var i=0; i<data[0].length; i++) {
                        $('#hpa_append_div').append('<div class="border pl-2 hpa_search_div_click">'+data[0][i]+'</div>');
                    }
                    hpa_data = data[1];
                    data_len = parseInt(data[2]);
                    buttons = data[3];

                    $('#pagination_buttons').empty();
                    $('#pagination_buttons').append(buttons);
                }
            }
        });
    });

    $('#hpa_append_div').on('click', '.hpa_search_div_click', function () {

        var this_html = $(this).html();
        $('#hpa_search_input').val(this_html);
        var start;

        for (var i=0; i<hpa_data.length; i++){
            if (hpa_data[i].hpa_title_on_of == this_html){
                start = i;
            }
        }

        data_len = data_len + start;
        var html = '';
        for (var i=start; i<data_len; i++){
            if (typeof hpa_data[i] == 'undefined') {
                break;
            }

            if (hpa_data[i].hpa_title_on_of != "undefined"){
                    html += '<h5 class="card-title text-center">'+hpa_data[i].hpa_title_on_of+'</h5>';
            }
            html += '<div class="card-body">';
            if (hpa_data[i].hpa_image_on_of != "undefined"){
                html += '<p class="card-text"><i>'+hpa_data[i].hpa_image_on_of+'</i></p>';
            }
            if (hpa_data[i].hpa_content_on_of != "undefined"){
                html += '<p class="card-text"><i>'+hpa_data[i].hpa_content_on_of+'</i></p>';
            }
            if (hpa_data[i].hpa_color_on_of != "undefined"){
                html += '<p class="card-text pt5 text-center border border-dark" style="background: '+hpa_data[i].hpa_color_on_of+';"><b>COLOR</b></p>'
                }
            if (hpa_data[i].hpa_width_on_of != "undefined"){
                html += '<p class="card-text"><b>Price: - </b>'+hpa_data[i].hpa_width_on_of+'</p>';
            }
            if (hpa_data[i].hpa_height_on_of != "undefined"){
                html += '<p class="card-text"><b>Width: - </b>'+hpa_data[i].hpa_height_on_of+'</p>';
            }
            if (hpa_data[i].hpa_price_on_of != "undefined"){
                html += '<p class="card-text"><b>Height: - </b>'+hpa_data[i].hpa_price_on_of+'</p>';
            }
            if (hpa_data[i].hpa_weight_on_of != "undefined"){
                html += '<p class="card-text"><b>Weight: - </b>'+hpa_data[i].hpa_weight_on_of+'</p>';
            }
            html += '</div>';
            
        }

        $('#hpa_for_remove').empty();
        $('#pagination_buttons').empty();
        $('#hpa_for_remove').append(html);
        $('#pagination_buttons').append(buttons);

        f1();
    });

    function f1(){
        var sea = $('#hpa_search_input').val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: hpa_prod_localize.url,
            data: {
                search: sea,
                action: 'hpa_div_click',
            },
            success: function(data){
                $('#pagination_buttons').empty();
                $('#pagination_buttons').append(data);
            }
        });
    }

    $('body').on('click', function (event) {
        if (event.target.id != 'hpa_search_input'){
            $('#hpa_append_div').empty();
        }
    });


    $('#upload_image').on('click', function (event) {

        event.preventDefault();

        var images = wp.media({
            title: "Upload Default Image For Product",
            multiple: false,
        });
        images.open().on('select', function () {
                var uploaded_images = images.state().get('selection');
                var selected_images = uploaded_images.toJSON();
                var url = selected_images[0].url;

                $.ajax({
                    type: 'POST',
                    url: hpa_prod_localize.url,
                    data: {
                        url: url,
                        action: 'hpa_update_image',
                    },
                    success: function(data){
                        $('#hpa_media_library').val(data);
                    }
                });
        });

    });

});