<?php

function codex_custom_init() {

    $args = array(
        'public'    => true,
        'label'     => 'Product',
        'supports'  => array( 'title', 'editor', 'thumbnail')
    );
    register_post_type( 'product', $args );
}
add_action( 'init', 'codex_custom_init' );

function add_product_type(){

    add_meta_box(
        'product_price',
        __( 'Add Price' ),
        'product_price',
        'product',
        'normal',
        'default'
    );

    add_meta_box(
        'product_width',
        __( 'Add Width' ),
        'product_width',
        'product',
        'normal',
        'default'
    );

    add_meta_box(
        'product_height',
        __( 'Add Height' ),
        'product_height',
        'product',
        'normal',
        'default'
    );

    add_meta_box(
        'product_weight',
        __( 'Add Weight' ),
        'product_weight',
        'product',
        'normal',
        'default'
    );

    add_meta_box(
        'product_color',
        __( 'Add Color' ),
        'product_color',
        'product',
        'normal',
        'default'
    );

}

add_action('add_meta_boxes', 'add_product_type');

function product_price(){
    global $post;
    $data = get_post_custom( $post->ID );
    $val = isset($data['price_input']) ? esc_attr($data['price_input'][0]) : '';

    echo '<input type="text", name="price_input" id="price_input" value="'.$val.'" placeholder="Enter Price">';
}

function product_width(){
    global $post;
    $data = get_post_custom( $post->ID );
    $val = isset($data['width_input']) ? esc_attr($data['width_input'][0]) : '';

    echo '<input type="text", name="width_input" id="width_input" value="'.$val.'" placeholder="Enter Width">';
}

function product_height(){
    global $post;
    $data = get_post_custom( $post->ID );
    $val = isset($data['height_input']) ? esc_attr($data['height_input'][0]) : '';


    echo '<input type="text", name="height_input" id="height_input" value="'.$val.'" placeholder="Enter Height">';
}

function product_weight(){
    global $post;
    $data = get_post_custom( $post->ID );
    $val = isset($data['weight_input']) ? esc_attr($data['weight_input'][0]) : '';

    echo '<input type="text", name="weight_input" id="weight_input" value="'.$val.'" placeholder="Enter Weight">';
}

function product_color(){
    global $post;
    $data = get_post_custom( $post->ID );
    $val = isset($data['color_input']) ? esc_attr($data['color_input'][0]) : '';

    echo '<input type="text", name="color_input" id="color_input" value="'.$val.'" placeholder="Enter Color">';
}

add_action('save_post', 'save_detail');

function save_detail(){
    global $post;

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post->ID;
    }

    update_post_meta($post->ID, 'image_input', $_POST['image_input']);
    update_post_meta($post->ID, 'price_input', $_POST['price_input']);
    update_post_meta($post->ID, 'width_input', $_POST['width_input']);
    update_post_meta($post->ID, 'height_input', $_POST['height_input']);
    update_post_meta($post->ID, 'weight_input', $_POST['weight_input']);
    update_post_meta($post->ID, 'color_input', $_POST['color_input']);

}