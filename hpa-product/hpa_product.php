<?php

/*
    Plugin Name: HPA Products
*/

require __DIR__ . '/hpa_widget.php';
require __DIR__ . '/hpa_register_post.php';
require __DIR__ . '/hpa_settings.php';
require __DIR__ . '/ajax_functions.php';

wp_enqueue_style('hpa-product-bootstrap', plugins_url( 'assets/bootstrap/css/bootstrap.css', __FILE__));
wp_enqueue_style('hpa-product-bootstrap', plugins_url( 'assets/bootstrap/js/bootstrap.js', __FILE__));
wp_enqueue_style('hpa-product-css', plugins_url( 'assets/css/style.css', __FILE__));
wp_enqueue_script('hpa-product-jquery', plugins_url( 'assets/hpa-js/hpa_js.js', __FILE__), array('jquery'));
wp_localize_script( 'hpa-product-jquery', 'hpa_prod_localize', ['url' => admin_url('admin-ajax.php')] );

function a($a){
    echo $a;
}