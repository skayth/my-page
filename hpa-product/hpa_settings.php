<?php

function hpa_add_new_menu_item(){
    add_menu_page(
        'Products Options',
        'Products Options',
        'manage_options',
        'product-options',
        'products_options_page'
    );
}

function products_options_page(){
    ?>
    <div class="wrap">
        <h1>Product Options</h1>
        <form method="post" action="options.php">
            <?php
            settings_fields('header_section');
            do_settings_sections('product-options');
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

add_action('admin_menu', 'hpa_add_new_menu_item');

function hpa_display_options(){

    add_settings_section(
        'header_section',
        '',
        '',
        'product-options'
    );

    add_settings_field(
        'hpa_title_on_of',
        'Title',
        'hpa_title_on_of',
        'product-options',
        'header_section'
    );

    add_settings_field(
        'hpa_content_on_of',
        'Content',
        'hpa_content_on_of',
        'product-options',
        'header_section'
    );

    add_settings_field(
        'hpa_image_on_of',
        'Image',
        'hpa_image_on_of',
        'product-options',
        'header_section'
    );

    add_settings_field(
        'hpa_color_on_of',
        'Color',
        'hpa_color_on_of',
        'product-options',
        'header_section'
    );

    add_settings_field(
        'hpa_width_on_of',
        'Width',
        'hpa_width_on_of',
        'product-options',
        'header_section'
    );

    add_settings_field(
        'hpa_height_on_of',
        'Height',
        'hpa_height_on_of',
        'product-options',
        'header_section'
    );

    add_settings_field(
        'hpa_price_on_of',
        'Price',
        'hpa_price_on_of',
        'product-options',
        'header_section'
    );

    add_settings_field(
        'hpa_weight_on_of',
        'Weight',
        'hpa_weight_on_of',
        'product-options',
        'header_section'
    );

    add_settings_field(
        'hpa_media_library',
        'Media',
        'hpa_media_library',
        'product-options',
        'header_section'
    );

    register_setting('header_section', 'hpa_title_on_of');
    register_setting('header_section', 'hpa_content_on_of');
    register_setting('header_section', 'hpa_image_on_of');
    register_setting('header_section', 'hpa_color_on_of');
    register_setting('header_section', 'hpa_width_on_of');
    register_setting('header_section', 'hpa_height_on_of');
    register_setting('header_section', 'hpa_price_on_of');
    register_setting('header_section', 'hpa_weight_on_of');
    register_setting('header_section', 'hpa_media_library');
}

function hpa_title_on_of(){
    echo '<input type="checkbox" id="hpa_title_on_of" name="hpa_title_on_of" value="1"'.checked('1', get_option('hpa_title_on_of'), false).'>';
}

function hpa_content_on_of(){
    echo '<input type="checkbox" id="hpa_content_on_of" name="hpa_content_on_of" value="1"'.checked('1', get_option('hpa_content_on_of'), false).'>';
}

function hpa_color_on_of(){
    echo '<input type="checkbox" id="hpa_color_on_of" name="hpa_color_on_of" value="1"'.checked('1', get_option('hpa_color_on_of'), false).'>';
}

function hpa_image_on_of(){
    echo '<input type="checkbox" id="hpa_image_on_of" name="hpa_image_on_of" value="1"'.checked('1', get_option('hpa_image_on_of'), false).'>';
}

function hpa_width_on_of(){
    echo '<input type="checkbox" id="hpa_width_on_of" name="hpa_width_on_of" value="1"'.checked('1', get_option('hpa_width_on_of'), false).'>';
}

function hpa_height_on_of(){
    echo '<input type="checkbox" id="hpa_height_on_of" name="hpa_height_on_of" value="1"'.checked('1', get_option('hpa_height_on_of'), false).'>';
}

function hpa_price_on_of(){
    echo '<input type="checkbox" id="hpa_price_on_of" name="hpa_price_on_of" value="1"'.checked('1', get_option('hpa_price_on_of'), false).'>';
}

function hpa_weight_on_of(){
    echo '<input type="checkbox" id="hpa_weight_on_of" name="hpa_weight_on_of" value="1"'.checked('1', get_option('hpa_weight_on_of'), false).'>';
}

function hpa_media_library(){
    echo '<button id="upload_image">Upload Image</button>';
    echo '<input type="hidden" id="hpa_media_library" name="hpa_media_library" value="'.get_option( 'hpa_media_library' ).'">';
    wp_enqueue_media();
}

add_action('admin_init', 'hpa_display_options');

